package test.awamo.marsroverkata;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MarsRoverKataApplicationTests {

//	Point point;
    private final int startLocation = 2;
    private final int endLocation = 4;
    
    
	@Test
	void contextLoads() {
//		point = new Point(startLocation, endLocation);
	}
    
    @Test
    public void newInstanceShouldSetStartLocationAndEndLocationParams() {
    	Point point = new Point(startLocation, endLocation);
        assertEquals(point.getStartLocation(), startLocation);
        assertEquals(point.getEndLocation(), endLocation);
    }
    
    @Test
    public void getForwardLocationShouldIncreasePointValueByOne() {
    	Point point = new Point(startLocation, endLocation);
        int expected = point.getStartLocation() + 1;
        assertEquals(point.getForwardLocation(), expected);
    }

    @Test
    public void getBackwardLocationShouldDecreasePointValueByOne() {
    	Point point = new Point(startLocation, endLocation);
        int expected = point.getStartLocation() - 1;
        assertEquals(point.getBackwardLocation(), expected);
    }

    @Test
    public void getBackwardLocationShouldSetValueToEndLocationIfZeroLocationIsPassed() {
    	Point point = new Point(startLocation, endLocation);
        point.setStartLocation(0);
        assertEquals(point.getBackwardLocation(), point.getEndLocation());
    }
}
