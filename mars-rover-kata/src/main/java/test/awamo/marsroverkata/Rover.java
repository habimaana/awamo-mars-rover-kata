package test.awamo.marsroverkata;

public class Rover {
	private Coordinates coordinates;

	public Rover(Coordinates coordinates) {
		super();
		this.coordinates = coordinates;
	}

	public Coordinates getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(Coordinates coordinates) {
		this.coordinates = coordinates;
	}
	
	public boolean process(char command) throws Exception {
		switch(Character.toUpperCase(command)) {
			case 'F':
				return coordinates.moveForward();
			case 'B':
				return coordinates.moveBackward();
			case 'L':
				coordinates.moveLeft();
				return true;
			case 'R':
				coordinates.moveRight();
				return true;
			default:
				throw new Exception("Unknown: "+command);
		}
	}
	
}
