package test.awamo.marsroverkata;

public class Point {
	private int startLocation;
	private int endLocation;
	
	public Point(int startLocation, int endLocation) {
		super();
		this.startLocation = startLocation;
		this.endLocation = endLocation;
	}
	
	public int getStartLocation() {
		return startLocation;
	}
	public void setStartLocation(int startLocation) {
		this.startLocation = startLocation;
	}
	public int getEndLocation() {
		return endLocation;
	}
	public void setEndLocation(int endLocation) {
		this.endLocation = endLocation;
	}
	
	public int getForwardLocation() {
        return (getStartLocation() + 1) % (getEndLocation() + 1);
    }

    public int getBackwardLocation() {
        if (getStartLocation() > 0) return getStartLocation() - 1;
        else return getEndLocation();
    }
}
