package test.awamo.marsroverkata;

public enum Direction {	
	NORTH, WEST, SOUTH, EAST;
	
	private int value;

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
