package test.awamo.marsroverkata;

public class Coordinates {
	private Point x;
	private Point y;
	private Direction direction;
	
	public Coordinates(Point x, Point y, Direction direction) {
		super();
		this.x = x;
		this.y = y;
		this.direction = direction;
	}
	
	public Point getX() {
		return x;
	}
	public void setX(Point x) {
		this.x = x;
	}
	public Point getY() {
		return y;
	}
	public void setY(Point y) {
		this.y = y;
	}
	public Direction getDirection() {
		return direction;
	}
	public void setDirection(Direction direction) {
		this.direction = direction;
	}
	
	public boolean moveForward() {
		y.getForwardLocation();
		return true;
	}
	
	public boolean moveBackward() {
		y.getBackwardLocation();
		return true;
	}
	
	public void moveLeft() {
		changeDirection(direction, -1);
	}
	
	public void moveRight() {
		changeDirection(direction, 1);
	}
	
	private void changeDirection(Direction direction, int directionStep) {
        int directions = Direction.values().length;
        int index = (directions + direction.getValue() + directionStep) % directions;
        direction = Direction.values()[index];
    }
	
}
